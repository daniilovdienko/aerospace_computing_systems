from unittest.mock import patch

import numpy as np
import pytest
from scipy.spatial.transform import Rotation

from magnetic import B_dot_body, magnetometer_measure

B_dot_body_test_data = [
    pytest.param([1, 0, 0], [1, 0, 0], [0, 0, 0], id="x rotation"),
    pytest.param([1, 0, 0], [0, 1, 0], [0, 0, 1], id="y rotation"),
    pytest.param([1, 0, 0], [0, 0, 1], [0, -1, 0], id="z rotation"),
    pytest.param([1, 0, 0], [5, 8, 1], [0, -1, 8], id="mixed rotation"),
    pytest.param([1, 0, 0], [0, 0, 0], [0, 0, 0], id="no rotation"),
]


@pytest.mark.parametrize(
    "magnetic_vector, sat_omega_body, expected_b_dot_vector", B_dot_body_test_data
)
def test_B_dot_body(magnetic_vector, sat_omega_body, expected_b_dot_vector):
    # все компоненты посчитанного вектора должны совпадать с соответствующими
    # компонентами ожидаемого вектора
    assert (
        B_dot_body(magnetic_vector, sat_omega_body) == np.array(expected_b_dot_vector)
    ).all()


# Тестовые данные для magnetometer_measure
magnetometer_measure_test_data = []

# Генерация пяти случайных тестовых данных
for _ in range(5):
    random_rotation_matrix = Rotation.random(random_state=123).as_matrix()
    random_vector = np.random.rand(3)
    random_magnetometer_test_data = pytest.param(
        random_rotation_matrix, random_vector, random_vector
    )
    magnetometer_measure_test_data.append(random_magnetometer_test_data)


@pytest.mark.parametrize(
    "random_rotation_matrix, B_body, expected_B_body_vector",
    magnetometer_measure_test_data,
)
def test_magnetometer_measure(random_rotation_matrix, B_body, expected_B_body_vector):
    with patch(
        "config.mission_parameters.MAGNET_MOUNT",
        new_callable=lambda: random_rotation_matrix,
    ):
        resvec = magnetometer_measure(B_body)
        test_vec = np.linalg.inv(random_rotation_matrix).dot(resvec)

    assert np.allclose(test_vec, expected_B_body_vector)
