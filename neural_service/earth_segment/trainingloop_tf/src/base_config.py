from dataclasses import dataclass


@dataclass
class Config:
    seed: int
    step_size: int
    lr: float
    gamma: float
    img_size: int
    channel: int
    batch_size: int
    n_epoch: int
    img_dir: str
    labels_dir: str
    device: str
    path_models: str
    augmentation: str
    project_name: str
    model_name: str
    img_test: str
    test_dir: str
    size_val: float
    size_test: float
    threshold: float
    classes: list
