import tensorflow as tf


def model_baseline(config, n_classes):
    input_shape = (config.img_size, config.img_size, config.channel)

    model = tf.keras.Sequential(
        [
            tf.keras.layers.Conv2D(
                16,
                (3, 3),
                activation="relu",
                kernel_initializer="he_normal",
                input_shape=input_shape,
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Conv2D(
                32, (3, 3), activation="relu", kernel_initializer="he_normal"
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Conv2D(
                64, (3, 3), activation="relu", kernel_initializer="he_normal"
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Conv2D(
                64, (3, 3), activation="relu", kernel_initializer="he_normal"
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(
                1024, activation="relu", kernel_initializer="he_normal"
            ),
            tf.keras.layers.Dense(n_classes, activation="sigmoid"),
        ],
        name="baseline",
    )

    return model


def pretrain_model(config, n_classes):
    input_shape = (config.img_size, config.img_size, config.channel)
    base_model = tf.keras.applications.mobilenet_v2.MobileNetV2(
        include_top=False, weights="imagenet", input_shape=input_shape
    )

    model = tf.keras.Sequential()

    model.add(tf.keras.layers.BatchNormalization(input_shape=input_shape))

    model.add(base_model)

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(n_classes, activation="sigmoid"))

    return model
